package us.lithub.data;

import java.util.Map;

public class ItemLookup {

	private ItemLookup() {

	}

	public static Book findProduct(String upc) {
		Map<String, String> bookMap = BookParser.getBookAttributesFromUpc(upc);

		Book book = new Book();
		book.setUpc(bookMap.get(upc));
		book.setTitle(bookMap.get(Book.TITLE));
		book.setAuthor(bookMap.get(Book.AUTHOR_TEXT));
		book.setIsbn(bookMap.get(Book.ISBN));
		book.setPublisher(bookMap.get(Book.PUBLISHER_TEXT));
		book.setSummary(bookMap.get(Book.SUMMARY));
		return book;
	}
}
