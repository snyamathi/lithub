package us.lithub.data;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import android.util.Log;

public class BookParser {
	
	private static final String TAG = BookParser.class.getSimpleName();
	
	public BookParser(){
		
	}
	
	public static Map<String, String> getBookAttributesFromIsbn(String isbn) {
		return getBookAttributesFromUpc(isbn);
	}

	public static Map<String, String> getBookAttributesFromUpc(String upc) {

		try {
			final URL url = new URL("http://isbndb.com/api/books.xml?access_key=MIIRXTQX&index1=isbn&value1=" + upc);
			final SAXParserFactory spf = SAXParserFactory.newInstance();
			final SAXParser sp = spf.newSAXParser();	
			final XMLReader xr = sp.getXMLReader();	
			final IsbnDbHandler handler = new IsbnDbHandler();
			xr.setContentHandler(handler);
			xr.parse(new InputSource(url.openStream()));
			return handler.getAttributes();
		} catch (MalformedURLException e) {
			Log.e(TAG, e.getMessage());
		} catch (SAXException e) {
			Log.e(TAG, e.getMessage());
		} catch (ParserConfigurationException e) {
			Log.e(TAG, e.getMessage());
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		}

		return null;
	}

	/**
	 * Handle the output form ISBNdb.com's REST API which allows us to get book information from the UPC code.
	 * 
	 * Sample output:
	 * 
	 * <ISBNdb server_time="2012-03-30T02:10:47Z">
	 * 	<BookList total_results="1" page_size="10" page_number="1" shown_results="1">
	 * 		<BookData book_id="loaded_dice_a04" isbn="0352320346" isbn13="9780352320346">
	 * 			<Title>Loaded dice</Title>
	 * 			<TitleLong>Loaded dice the true story of a casino cheat</TitleLong>
	 * 			<AuthorsText>John Soares</AuthorsText>
	 * 			<PublisherText publisher_id="w_h_allen_a01">London W.H. Allen 1988, c1985</PublisherText>
	 * 			<Summary>This is a summary about the book</Summary>
	 * 		</BookData>
	 * 	</BookList>
	 * </ISBNdb>
	 */
	private static class IsbnDbHandler extends DefaultHandler {

		private Map<String, String> map;

		private boolean Title;
		private boolean TitleLong;
		private boolean AuthorsText;
		private boolean PublisherText;
		private boolean Summary;

		public IsbnDbHandler() {
			map = new HashMap<String, String>();
		}

		public Map<String, String> getAttributes() {
			return map;
		}

		@Override
		public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException {
			if (localName.equals("BookData")) {
				String isbn = atts.getValue(Book.ISBN);
				map.put(Book.ISBN, isbn);
			} else if (localName.equals(Book.TITLE)) {
				Title = true;
			} else if (localName.equals(Book.TITLE_LONG)) {
				TitleLong = true;
			} else if (localName.equals(Book.AUTHOR_TEXT)) {
				AuthorsText = true;
			} else if (localName.equals(Book.PUBLISHER_TEXT)) {
				PublisherText = true;
			} else if (localName.equals(Book.SUMMARY)) {
				Summary = true;
			}
		}

		@Override
		public void characters(char ch[], int start, int length) throws SAXException {
			if (Title) {
				map.put(Book.TITLE, new String(ch, start, length));
				Title = false;
			} else if (TitleLong) {
				map.put(Book.TITLE_LONG, new String(ch, start, length));
				TitleLong = false;
			} else if (AuthorsText) {
				map.put(Book.AUTHOR_TEXT, new String(ch, start, length).replaceAll(",", ""));
				AuthorsText = false;
			} else if (PublisherText) {
				map.put(Book.PUBLISHER_TEXT, new String(ch, start, length));
				PublisherText = false;
			} else if (Summary) {
				map.put(Book.SUMMARY, new String (ch, start, length));
				Summary = false;
			}
		}
	}
}
