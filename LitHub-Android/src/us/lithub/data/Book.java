package us.lithub.data;

public class Book {
	
	public static final String AUTHOR_TEXT = "AuthorsText";
	public static final String TITLE = "Title";
	public static final String TITLE_LONG = "TitleLong";
	public static final String ISBN = "isbn";
	public static final String PUBLISHER_TEXT = "PublisherText";
	public static final String SUMMARY = "Summary";
	public static final String UPC = "UPC";
	
	private int amazonRating;
	private float myRating;
	private int status;
	private long datePublished;
	private boolean isFavorite;
	private String amazonURL;
	private String author;
	private String publisher;
	private String summary;
	private String isbn;
	private String upc;
	private String title;
	private String version;
	
	public Book() {
		
	}

	public int getAmazonRating() {
		return amazonRating;
	}

	public void setAmazonRating(int amazonRating) {
		this.amazonRating = amazonRating;
	}

	public float getMyRating() {
		return myRating;
	}

	public void setMyRating(float myRating) {
		this.myRating = myRating;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public void setPublisher(String publisher){
		this.publisher = publisher;
	}
	
	public String getPublisher(){
		return this.publisher;
	}
	
	public void setSummary(String summary){
		this.summary = summary;
	}
	
	public String getSummary(){
		return this.summary;
	}
	
	public long getDatePublished() {
		return datePublished;
	}

	public void setDatePublished(long datePublished) {
		this.datePublished = datePublished;
	}

	public boolean isFavorite() {
		return isFavorite;
	}

	public void setFavorite(boolean isFavorite) {
		this.isFavorite = isFavorite;
	}

	public String getAmazonURL() {
		return amazonURL;
	}

	public void setAmazonURL(String amazonURL) {
		this.amazonURL = amazonURL;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getUpc() {
		return upc;
	}

	public void setUpc(String upc) {
		this.upc = upc;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}
	
	public static Book getDummyData() {
		Book book = new Book();
		book.setUpc("9780385504225");
		book.setTitle("The Lost Symbol");
		book.setAuthor("Dan Brown");
		book.setVersion("1");
		book.setStatus(0);
		book.setIsbn("9780385504225");
		book.setAmazonRating(3);
		book.setAmazonURL("http://www.amazon.com/Lost-Symbol-Dan-Brown/dp/1400079144/");
		book.setDatePublished(System.currentTimeMillis());
		book.setFavorite(true);
		book.setMyRating(4);
		return book;
	}
}
