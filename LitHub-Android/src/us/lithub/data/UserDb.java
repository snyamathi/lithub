package us.lithub.data;

import us.lithub.Util;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class UserDb {
	
	private static final String TAG = UserDb.class.getSimpleName();
	
	public static final String TABLE_USER_BOOKS = "user_books";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_ISBN = "isbn";
	public static final String COLUMN_STATUS = "status";
	public static final String COLUMN_IS_FAVORITE = "isFavorite";
	public static final String COLUMN_MY_RATING = "myRating";
	public static final String COLUMN_DATE_STARRED = "dateStarred";
	public static final String COLUMN_DATE_ADDED = "dateAdded";
	
	public static final String TABLE_FRIENDS = "friends";
	public static final String COLUMN_FRIEND_EMAIL = "friend_email";
	public static final String COLUMN_FRIEND_ALIAS = "alias";
	public static final String COLUMN_FRIEND_DISPLAY_NAME = "display_name";
	
	public static final String TABLE_TRANSACTIONS = "transactions";
	public static final String COLUMN_START_DATE = "startDate";
	public static final String COLUMN_END_DATE = "endDate";
	
	private static final String QUERY_ALL = "SELECT global_books._id, global_books.isbn, global_books.author, global_books.title, user_books.status, user_books.isFavorite FROM global_books JOIN user_books ON user_books.isbn=global_books.isbn";
	private static final String QUERY_ALL_FRIENDS = "SELECT * FROM "+TABLE_FRIENDS;
	private static final String QUERY_ALL_TRANSACTIONS = "SELECT * FROM "+TABLE_FRIENDS;
	
	private SQLiteDatabase mDatabase;
	private UserDbOpenHelper dbHelper;
	private Context mContext;
	private boolean databaseAttached;

	public UserDb(Context context) {
		dbHelper = new UserDbOpenHelper(context);
		this.mContext = context;
	}

	public void open() throws SQLException {
		mDatabase = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}
	
	public void initialize() {			
		open();
		for (String isbn : Util.ISBN_LIST) {
			addBook(isbn);
		}
		close();
	}
	
	private void attachDatabases() {
		mDatabase.execSQL("attach database ? as GlobalDB", new String[]{ mContext.getDatabasePath("GlobalDB").getPath() });
		databaseAttached = true;
	}
	
	/**
	 * Query over all books that are in the user database joined on the global database.  In other words, we are cross-referencing
	 * the user database to get which books we want to display, but getting the information to display from the global list of books.
	 * @return
	 */
	public Cursor getAllBooks() {		
		if (!databaseAttached) {
			attachDatabases();
		}
		Cursor cursor = mDatabase.rawQuery(QUERY_ALL, null);
		if (cursor == null) {
			Log.e(TAG, "GetAllBooks query failed");
			// TODO handle this exception
		}	
		return cursor;
	}
	
	public void deleteBook(String isbn) {
		mDatabase.execSQL("DELETE FROM user_books WHERE isbn='"+isbn+"'");
	}
	
	public String getBookTitle(long _id) {
		if (!databaseAttached) {
			attachDatabases();
		}
		Cursor cursor = mDatabase.rawQuery("SELECT * FROM global_books WHERE _id='"+_id+"'" , null);
		if (cursor == null || cursor.getCount() != 1) {
			Log.e(TAG, "getBook query failed");
			// TODO handle this exception
		}	
		
		cursor.moveToFirst();
		return cursor.getString(cursor.getColumnIndex(GlobalDb.COLUMN_TITLE));
	}
	
	public boolean isBookCurrentlyHeld(String isbn) {
		if (!databaseAttached) {
			attachDatabases();
		}
		Cursor cursor = mDatabase.rawQuery("SELECT "+COLUMN_STATUS+" FROM "+TABLE_USER_BOOKS+" WHERE "+COLUMN_ISBN+"='"+isbn+"'", null);
		
		// Move the cursor to the first row
		if (!cursor.moveToFirst()) {
			return false;
		}	
		
		// Get the status of the book
		int status = cursor.getInt(cursor.getColumnIndex(COLUMN_STATUS));
		if (status == 101) {
			return true;
		} else {
			return false;
		}
	}

	public Cursor getAllFriends() {
		Cursor cursor = mDatabase.rawQuery(QUERY_ALL_FRIENDS, null);
		if (cursor == null) {
			Log.e(TAG, "GetAllFriends query failed");
			// TODO handle this exception
		}	
		return cursor;
	}
	
	public Cursor getAllTransactions() {
		Cursor cursor = mDatabase.rawQuery(QUERY_ALL_TRANSACTIONS, null);
		if (cursor == null) {
			Log.e(TAG, "GetAllTransactions query failed");
			// TODO handle this exception
		}	
		return cursor;
	}

	/**
	 * Add a new book to the user's library
	 * @param isbn the ISBN number of the book to add
	 * @return the row ID of the newly inserted row, or -1 if an error occurred 
	 */
	public long addBook(String isbn) {
		ContentValues values = new ContentValues();
		values.put(UserDb.COLUMN_ISBN, isbn);
		values.put(UserDb.COLUMN_IS_FAVORITE, 0);
		values.put(UserDb.COLUMN_MY_RATING, 0);
		values.put(UserDb.COLUMN_STATUS, 0);
		return mDatabase.insert(UserDb.TABLE_USER_BOOKS, null, values);
	}
	
	public long addFriend(String friendEmail, String friendDisplayName) {
		ContentValues values = new ContentValues();
		values.put(UserDb.COLUMN_FRIEND_EMAIL, friendEmail);
		values.put(UserDb.COLUMN_FRIEND_DISPLAY_NAME, friendDisplayName);
		values.put(UserDb.COLUMN_FRIEND_ALIAS, friendDisplayName);
		values.put(UserDb.COLUMN_IS_FAVORITE, 0);
		return mDatabase.insert(UserDb.TABLE_FRIENDS, null, values);
	}
	
	public long addFriend(String friendEmail, String friendDisplayName, String friendAlias, int isFavorite) {
		ContentValues values = new ContentValues();
		values.put(UserDb.COLUMN_FRIEND_EMAIL, friendEmail);
		values.put(UserDb.COLUMN_FRIEND_DISPLAY_NAME, friendDisplayName);
		values.put(UserDb.COLUMN_FRIEND_ALIAS, friendAlias);
		values.put(UserDb.COLUMN_IS_FAVORITE, isFavorite);
		return mDatabase.insert(UserDb.TABLE_FRIENDS, null, values);
	}
	
	public long addTransaction(String friendsEmailAddress, String isbn, int status) {
		ContentValues values = new ContentValues();
		values.put(UserDb.COLUMN_FRIEND_EMAIL, friendsEmailAddress);
		values.put(UserDb.COLUMN_ISBN, isbn);
		values.put(UserDb.COLUMN_STATUS, status);
		return mDatabase.insert(UserDb.TABLE_TRANSACTIONS, null, values);
	}

	private class UserDbOpenHelper extends SQLiteOpenHelper {

		private static final String DB_NAME = "UserDB";
		private static final int DB_VERSION = 1;
		private static final String USER_BOOKS_TABLE_CREATE = "CREATE TABLE " + TABLE_USER_BOOKS + " (" + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COLUMN_ISBN + " TEXT, " + COLUMN_STATUS + " INTEGER, " + COLUMN_MY_RATING + " TEXT, " + COLUMN_IS_FAVORITE + " INTEGER, " + COLUMN_DATE_STARRED + " DATE, " + COLUMN_DATE_ADDED + " DATE);";
		private static final String FRIENDS_TABLE_CREATE = "CREATE TABLE " + TABLE_FRIENDS + " (" + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COLUMN_FRIEND_EMAIL + " TEXT, " + COLUMN_FRIEND_DISPLAY_NAME + " TEXT, " + COLUMN_FRIEND_ALIAS + " TEXT, " + COLUMN_IS_FAVORITE + " INTEGER);";
		private static final String TRANSACTIONS_TABLE_CREATE = "CREATE TABLE " + TABLE_TRANSACTIONS + " (" + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COLUMN_FRIEND_EMAIL + " TEXT, " + COLUMN_ISBN + " TEXT, " + COLUMN_STATUS + " INTEGER, " + COLUMN_START_DATE + " DATE, " + COLUMN_END_DATE + " DATE);";
		
		public UserDbOpenHelper(Context context) {
			super(context, DB_NAME, null, DB_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase database) {
			database.execSQL(USER_BOOKS_TABLE_CREATE);
			database.execSQL(FRIENDS_TABLE_CREATE);
			database.execSQL(TRANSACTIONS_TABLE_CREATE);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			throw new UnsupportedOperationException();
		}
	}
}
