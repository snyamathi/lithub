/*
 * Copyright 2011 Google Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package us.lithub;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import us.lithub.appengine.AndroidRequestTransport;
import us.lithub.appengine.Setup;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.AssetManager;
import android.util.Log;

import com.fedorvlasov.lazylist.ImageLoader;
import com.google.web.bindery.event.shared.SimpleEventBus;
import com.google.web.bindery.requestfactory.shared.RequestFactory;
import com.google.web.bindery.requestfactory.vm.RequestFactorySource;

/**
 * Utility methods for getting the base URL for client-server communication and
 * retrieving shared preferences.
 */
public class Util {
	
	public static final int PENDING = 0;
	public static final int IN_PROGRESS = 1;
	public static final int COMPLETE = 2;

    /**
     * Tag for logging.
     */
    private static final String TAG = "Util";

    // Shared constants

    /**
     * Key for account name in shared preferences.
     */
    public static final String ACCOUNT_NAME = "accountName";

    /**
     * Key for auth cookie name in shared preferences.
     */
    public static final String AUTH_COOKIE = "authCookie";

    /**
     * Key for connection status in shared preferences.
     */
    public static final String CONNECTION_STATUS = "connectionStatus";

    /**
     * Value for {@link #CONNECTION_STATUS} key.
     */
    public static final String CONNECTED = "connected";

    /**
     * Value for {@link #CONNECTION_STATUS} key.
     */
    public static final String CONNECTING = "connecting";

    /**
     * Value for {@link #CONNECTION_STATUS} key.
     */
    public static final String DISCONNECTED = "disconnected";
    
    public static final String DISPLAY_NAME = "display_name";

    /**
     * Key for device registration id in shared preferences.
     */
    public static final String DEVICE_REGISTRATION_ID = "deviceRegistrationID";

    /*
     * URL suffix for the RequestFactory servlet.
     */
    public static final String RF_METHOD = "/gwtRequest";

    /**
     * An intent name for receiving registration/unregistration status.
     */
    public static final String UPDATE_UI_INTENT = getPackageName() + ".UPDATE_UI";

    // End shared constants

    /**
     * Key for shared preferences.
     */
    private static final String SHARED_PREFS = "lithub".toUpperCase(Locale.ENGLISH) + "_PREFS";

    /**
     * Cache containing the base URL for a given context.
     */
    private static final Map<Context, String> URL_MAP = new HashMap<Context, String>();

    /**
     * Display a notification containing the given string.
     */
    public static void generateNotification(Context context, String message) {
        int icon = R.drawable.status_icon;
        long when = System.currentTimeMillis();

        Notification notification = new Notification(icon, message, when);
        notification.setLatestEventInfo(context, "C2DM Example", message,
                PendingIntent.getActivity(context, 0, null, PendingIntent.FLAG_CANCEL_CURRENT));
        notification.flags |= Notification.FLAG_AUTO_CANCEL;

        SharedPreferences settings = Util.getSharedPreferences(context);
        int notificatonID = settings.getInt("notificationID", 0);

        NotificationManager nm = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        nm.notify(notificatonID, notification);

        SharedPreferences.Editor editor = settings.edit();
        editor.putInt("notificationID", ++notificatonID % 32);
        editor.commit();
    }

    /**
     * Returns the (debug or production) URL associated with the registration
     * service.
     */
    public static String getBaseUrl(Context context) {
        String url = URL_MAP.get(context);
        if (url == null) {
            // if a debug_url raw resource exists, use its contents as the url
            url = getDebugUrl(context);
            // otherwise, use the production url
            if (url == null) {
                url = Setup.PROD_URL;
            }
            URL_MAP.put(context, url);
        }
        return url;
    }

    /**
     * Creates and returns an initialized {@link RequestFactory} of the given
     * type.
     */
    public static <T extends RequestFactory> T getRequestFactory(Context context,
            Class<T> factoryClass) {
        T requestFactory = RequestFactorySource.create(factoryClass);

        SharedPreferences prefs = getSharedPreferences(context);
        String authCookie = prefs.getString(Util.AUTH_COOKIE, null);

        String uriString = Util.getBaseUrl(context) + RF_METHOD;
        URI uri;
        try {
            uri = new URI(uriString);
        } catch (URISyntaxException e) {
            Log.w(TAG, "Bad URI: " + uriString, e);
            return null;
        }
        requestFactory.initialize(new SimpleEventBus(),
                new AndroidRequestTransport(uri, authCookie));

        return requestFactory;
    }

    /**
     * Helper method to get a SharedPreferences instance.
     */
    public static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences(SHARED_PREFS, 0);
    }

    /**
     * Returns true if we are running against a dev mode appengine instance.
     */
    public static boolean isDebug(Context context) {
        // Although this is a bit roundabout, it has the nice side effect
        // of caching the result.
        return !Setup.PROD_URL.equals(getBaseUrl(context));
    }

    /**
     * Returns a debug url, or null. To set the url, create a file
     * {@code assets/debugging_prefs.properties} with a line of the form
     * 'url=http:/<ip address>:<port>'. A numeric IP address may be required in
     * situations where the device or emulator will not be able to resolve the
     * hostname for the dev mode server.
     */
    private static String getDebugUrl(Context context) {
        BufferedReader reader = null;
        String url = null;
        try {
            AssetManager assetManager = context.getAssets();
            InputStream is = assetManager.open("debugging_prefs.properties");
            reader = new BufferedReader(new InputStreamReader(is));
            while (true) {
                String s = reader.readLine();
                if (s == null) {
                    break;
                }
                if (s.startsWith("url=")) {
                    url = s.substring(4).trim();
                    break;
                }
            }
        } catch (FileNotFoundException e) {
            // O.K., we will use the production server
            return null;
        } catch (Exception e) {
            Log.w(TAG, "Got exception " + e);
            Log.w(TAG, Log.getStackTraceString(e));
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    Log.w(TAG, "Got exception " + e);
                    Log.w(TAG, Log.getStackTraceString(e));
                }
            }
        }

        return url;
    }

    /**
     * Returns the package name of this class.
     */
    private static String getPackageName() {
        return Util.class.getPackage().getName();
    }
    
    // ADDED BY LITHUB:
    
	private static ImageLoader imageLoader;
	
	public static ImageLoader getImageLoader(Context context) {
		
		if (imageLoader == null) {
			imageLoader = new ImageLoader(context);
		}
		return imageLoader;
	}
	

	
    
    public static boolean isBackgroundSyncEnabled(Context context) {
    	SharedPreferences prefs = Util.getSharedPreferences(context);
    	return prefs.getBoolean("isBackgroundSyncEnabled", true);
    }
    
    public static void setBackgroundSyncEnabled(Context context, boolean isEnabled) {
    	SharedPreferences prefs = Util.getSharedPreferences(context);
    	Editor editor = prefs.edit();
    	editor.putBoolean("isBackgroundSyncEnabled", isEnabled);
    	editor.commit();
    }
	

	
	public static final String[] ISBN_LIST = new String[] {
		"9780060826598",
		"9780061068249",
		"9780061708770",
		"9780062049643",
		"9780062101891",
		"9780066620992",
		"9780071636087",
		"9780071748759",
		"9780123838629",
		"9780140256352",
		"9780140284751",
		"9780152066086",
		"9780152167905",
		"9780195337433",
		"9780199549405",
		"9780231064811",
		"9780262193191",
		"9780271009421",
		"9780275989231",
		"9780300074970",
		"9780300119282",
		"9780306428821",
		"9780306452420",
		"9780306458330",
		"9780306462009",
		"9780307593290",
		"9780307888129",
		"9780309041393",
		"9780309064187",
		"9780310210474",
		"9780310220206",
		"9780310321149",
		"9780310499015",
		"9780310712480",
		"9780312177065",
		"9780312238315",
		"9780312261337",
		"9780312329839",
		"9780312426637",
		"9780312499617",
		"9780312579975",
		"9780312590826",
		"9780313251948",
		"9780313284083",
		"9780313320071",
		"9780313324741",
		"9780313329999",
		"9780316043403",
		"9780316323949",
		"9780321649898",
		"9780345396730",
		"9780345495013",
		"9780345507327",
		"9780373513697",
		"9780374200138",
		"9780374288907",
		"9780375705267",
		"9780375725609",
		"9780385040259",
		"9780385240215",
		"9780385516310",
		"9780385520188",
		"9780385524797",
		"9780385528078",
		"9780387095721",
		"9780387208817",
		"9780387244709",
		"9780387311654",
		"9780387699349",
		"9780387749778",
		"9780387878614",
		"9780387902791",
		"9780387983202",
		"9780387985602",
		"9780389209263",
		"9780393064476",
		"9780393325096",
		"9780393705461",
		"9780394820385",
		"9780395536391",
		"9780395633670",
		"9780395935422",
		"9780415032575",
		"9780415062251",
		"9780415090483",
		"9780415112857",
		"9780415152297",
		"9780415165754",
		"9780415205146",
		"9780415220132",
		"9780415269391",
		"9780415278911",
		"9780415286596",
		"9780415297677",
		"9780415313827",
		"9780415341196",
		"9780415342117",
		"9780415369077",
		"9780415385756",
		"9780415449601",
		"9780415466660",
		"9780415473545",
		"9780415474627",
		"9780415911283",
		"9780415914192",
		"9780415914598",
		"9780415950503",
		"9780415952453",
		"9780415970662",
		"9780416399707",
		"9780416408409",
		"9780416737806",
		"9780419248903",
		"9780435037864",
		"9780439355407",
		"9780439813785",
		"9780443068553",
		"9780444518538",
		"9780444997128",
		"9780446576437",
		"9780465012626",
		"9780465091263",
		"9780470392553",
		"9780470457528",
		"9780470515976",
		"9780470548141",
		"9780470550472",
		"9780470612620",
		"9780470643167",
		"9780470741498",
		"9780470927625",
		"9780471110583",
		"9780471212119",
		"9780471310433",
		"9780471356592",
		"9780471389293",
		"9780471668275",
		"9780471910916",
		"9780472112876",
		"9780472114009",
		"9780486212357",
		"9780486409887",
		"9780486410739",
		"9780486423135",
		"9780495012665",
		"9780495600794",
		"9780520023772",
		"9780520024274",
		"9780520034600",
		"9780520069411",
		"9780520098565",
		"9780520218932",
		"9780521037914",
		"9780521047739",
		"9780521047968",
		"9780521076159",
		"9780521080767",
		"9780521192965",
		"9780521211918",
		"9780521221801",
		"9780521254199",
		"9780521281898",
		"9780521314022",
		"9780521401401",
		"9780521401715",
		"9780521423168",
		"9780521520317",
		"9780521524568",
		"9780521556569",
		"9780521587044",
		"9780521590181",
		"9780521646796",
		"9780521785488",
		"9780521806152",
		"9780521815949",
		"9780521819480",
		"9780521826235",
		"9780521849630",
		"9780521866118",
		"9780522856736",
		"9780525952305",
		"9780525952442",
		"9780531168127",
		"9780545202350",
		"9780553212778",
		"9780554217826",
		"9780554324272",
		"9780554328553",
		"9780554336879",
		"9780554370149",
		"9780566074455",
		"9780595241217",
		"9780596003722",
		"9780609802816",
		"9780618271917",
		"9780631231349",
		"9780631232841",
		"9780632050338",
		"9780632064564",
		"9780643069022",
		"9780664223922",
		"9780664224516",
		"9780664227562",
		"9780671510992",
		"9780674012479",
		"9780674035805",
		"9780674140462",
		"9780674802681",
		"9780679763888",
		"9780689312434",
		"9780691020754",
		"9780691083940",
		"9780691125602",
		"9780700615261",
		"9780700712335",
		"9780702166396",
		"9780715140710",
		"9780715175842",
		"9780715323663",
		"9780715324080",
		"9780716734840",
		"9780736811927",
		"9780736848800",
		"9780736857406",
		"9780738514253",
		"9780738519029",
		"9780738530840",
		"9780738857756",
		"9780739115428",
		"9780740758386",
		"9780748731961",
		"9780748774654",
		"9780749460563",
		"9780754609476",
		"9780754609698",
		"9780754632894",
		"9780754667810",
		"9780756660925",
		"9780756683702",
		"9780761419112",
		"9780761439905",
		"9780761439967",
		"9780761830429",
		"9780761958772",
		"9780764159558",
		"9780764159558",
		"9780765801845",
		"9780766178328",
		"9780768427295",
		"9780769218090",
		"9780781766456",
		"9780784717639",
		"9780784718391",
		"9780786024803",
		"9780786405398",
		"9780786406326",
		"9780786424580",
		"9780786672967",
		"9780787946678",
		"9780787987138",
		"9780787988418",
		"9780788023064",
		"9780791086148",
		"9780791406908",
		"9780791446300",
		"9780791447024",
		"9780791463802",
		"9780792331858",
		"9780792345671",
		"9780792354536",
		"9780792354994",
		"9780792363620",
		"9780792370208",
		"9780792372882",
		"9780792387879",
		"9780794602628",
		"9780801485411",
		"9780801487163",
		"9780801495441",
		"9780802043627",
		"9780802142894",
		"9780802448507",
		"9780802796370",
		"9780802849878",
		"9780803282773",
		"9780803733626",
		"9780804715492",
		"9780805075458",
		"9780805821833",
		"9780805834147",
		"9780805853810",
		"9780806524085",
		"9780806524900",
		"9780806526225",
		"9780807529980",
		"9780807844076",
		"9780809094660",
		"9780809139033",
		"9780809142569",
		"9780810151789",
		"9780811200066",
		"9780811212120",
		"9780811216067",
		"9780811731652",
		"9780812975246",
		"9780813332611",
		"9780816606412",
		"9780821844656",
		"9780822347651",
		"9780823026197",
		"9780825410000",
		"9780826215512",
		"9780835607810",
		"9780851994215",
		"9780874778434",
		"9780878148622",
		"9780892064410",
		"9780914386261",
		"9780915256402",
		"9780919123663",
		"9780929895284",
		"9780930261146",
		"9780931464782",
		"9780931674525",
		"9780935278583",
		"9780964472730",
		"9780970473325",
		"9781400049622",
		"9781400064588",
		"9781439192818",
		"9781451610369",
		"9781571104298",
		"9781571107282",
		"9781584350804",
		"9781596982925",
		"9781598531077",
		"9781617750250"
	};

	public static final String MODE = "mode";
	
	public static boolean isRegistered(Context context) {
        SharedPreferences prefs = getSharedPreferences(context);
        String deviceRegistrationID = prefs.getString(Util.DEVICE_REGISTRATION_ID, null);
        
        if (deviceRegistrationID == null) {
        	return false;
        }
        
        return true;
	}
}
