package us.lithub.ui.activity;

import us.lithub.R;
import us.lithub.Util;
import us.lithub.data.GlobalDb;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class Settings extends Activity {

	private static final String TAG = Settings.class.getSimpleName();
	
	private CheckBox chkBackgroundSync;
	private Context mContext = this;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);

		// Set the checkbox state depending on if we can do background sync
		chkBackgroundSync = (CheckBox) findViewById(R.id.chkBackgroundSync);
		chkBackgroundSync.setChecked(Util.isBackgroundSyncEnabled(this));
	}

	public void toggleSync(View v) {
		if (Util.isBackgroundSyncEnabled(this)) {
			chkBackgroundSync.setChecked(false);
			Util.setBackgroundSyncEnabled(this, false);
		} else {
			chkBackgroundSync.setChecked(true);
			Util.setBackgroundSyncEnabled(this, true);
		}
	}

	public void register(View view) {
		SharedPreferences prefs = Util.getSharedPreferences(mContext);
		String connectionStatus = prefs.getString(Util.CONNECTION_STATUS, Util.DISCONNECTED);
		Log.d(TAG, "Connection status = " + connectionStatus);
		if (Util.DISCONNECTED.equals(connectionStatus)) {
			startActivity(new Intent(this, Register.class));
		} else {
			Toast.makeText(this, R.string.already_registered, Toast.LENGTH_SHORT).show();
		}
	}
	
	public void clearCache(View view) {
		Util.getImageLoader(this).clearCache();
		Toast.makeText(this, "Cache cleared", Toast.LENGTH_SHORT).show();
	}
	
	public void updateCache(View view) {
		final Dialog homeSetup = new Dialog(mContext);	
		homeSetup.setContentView(R.layout.dialog_home_setup);
		homeSetup.setTitle("Initial Setup");
		homeSetup.show();

		// handler for the background updating
		final Handler muniHandler = new Handler() {
			TextView status = (TextView) homeSetup.findViewById(R.id.status);
			ProgressBar progress = (ProgressBar) homeSetup.findViewById(R.id.progressBar);
			int mStatus;
			public void handleMessage(Message msg) {
				if (mStatus == Util.PENDING) {
					mStatus = Util.IN_PROGRESS;
					status.setTypeface(null, Typeface.BOLD);
					status.setText("In Progress");
					progress.setProgress(0);
					progress.setMax(msg.arg1);
				} else {
					progress.setProgress(msg.arg2);
					if (msg.arg1 == msg.arg2) {
						mStatus = Util.COMPLETE;
						status.setTypeface(null, Typeface.NORMAL);
						status.setTextColor(Color.GREEN);
						status.setText("Complete");
						homeSetup.dismiss();
					}
				}
			}
		};
		
		GlobalDb db = new GlobalDb(this);
		db.hardReset(muniHandler);
	}
}
