package us.lithub.ui.activity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import us.lithub.R;
import us.lithub.Util;
import us.lithub.data.GlobalDb;
import us.lithub.data.UserDb;
import us.lithub.ui.NowPlayingInfo;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.markupartist.android.widget.ActionBar;

public class MainMenu extends Activity {

	private static final String TAG = MainMenu.class.getSimpleName();

	private final Context mContext = this;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_menu);        
		
		foo();
		bar();
		
		// checkVersion();
		// new FetchNowPlayingInfoTask().execute();
	}

	public void openMyLibrary(View v) {
		startActivity(new Intent(mContext, MyLibrary.class));
	}

	public void openScanBook(View v) {
		startActivity(new Intent(mContext, ScanBook.class));
	}

	public void openFriends(View v) {
		startActivity(new Intent(mContext, Friends.class));
	}

	public void openLending(View v) {
		startActivity(new Intent(mContext, Lending.class));
	}

	public void openDiscover(View v) {
		startActivity(new Intent(mContext, Discover.class));
	}

	public void openSettings(View v) {
		startActivity(new Intent(mContext, Settings.class));
	}

	private void checkVersion() {
		try {
			final PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_META_DATA);
			final SharedPreferences prefs = getPreferences(Context.MODE_PRIVATE);
			final int lastRunVersionCode = prefs.getInt("lastRunVersionCode", 0);

			if (lastRunVersionCode == 0) {
				Log.d(TAG, "New install");
				newInstall();

			} else if (lastRunVersionCode < pInfo.versionCode) {
				Log.d(TAG, "New update");
				newUpdate();
			} else {
				Log.d(TAG, "Current");
				//current();
				foo();
				current();
				bar();
				return;
			}

			final Editor editor = prefs.edit();
			editor.putInt("lastRunVersionCode", pInfo.versionCode);
			editor.commit();
		} catch (NameNotFoundException e) {
			Log.e(TAG, "Error checking version: " + e.getMessage());
		}
	}

	private void newInstall() {
		final Dialog homeSetup = new Dialog(mContext);	
		homeSetup.setContentView(R.layout.dialog_home_setup);
		homeSetup.setTitle("Initial Setup");
		homeSetup.show();

		// handler for the background updating
		final Handler muniHandler = new Handler() {
			TextView status = (TextView) homeSetup.findViewById(R.id.status);
			ProgressBar progress = (ProgressBar) homeSetup.findViewById(R.id.progressBar);
			int mStatus;
			public void handleMessage(Message msg) {
				if (mStatus == Util.PENDING) {
					mStatus = Util.IN_PROGRESS;
					status.setTypeface(null, Typeface.BOLD);
					status.setText("In Progress");
					progress.setProgress(0);
					progress.setMax(msg.arg1);
				} else {
					progress.setProgress(msg.arg2);
					if (msg.arg1 == msg.arg2) {
						mStatus = Util.COMPLETE;
						status.setTypeface(null, Typeface.NORMAL);
						status.setTextColor(Color.GREEN);
						status.setText("Complete");
						homeSetup.dismiss();
					}
				}
			}
		};

		new Thread(new Runnable() {
			public void run() {
				new UserDb(mContext).initialize();
				new GlobalDb(mContext).downloadDatabase(muniHandler);
			}
		}).start();;
	}

	private void newUpdate() {

	}

	private void current() {
		
	}

	private class FetchNowPlayingInfoTask extends AsyncTask<Void, Void, String> {

		private ActionBar actionbar;
		private WebView webview;

		@Override
		protected void onPreExecute () {
			actionbar = (ActionBar) findViewById(R.id.actionbar);
			actionbar.setProgressBarVisibility(View.VISIBLE);

			webview = (WebView) findViewById(R.id.webview);
			webview.setBackgroundColor(0x00000000);
			webview.loadData("<font color=\"white\"<center>Loading...</center>", "text/html", "UTF-8");
		}

		@Override
		protected String doInBackground(Void... arg0) {
			NowPlayingInfo info = new NowPlayingInfo();
			return info.getHtml();
		}

		@Override
		protected void onPostExecute(String html) {
			webview.loadData(html, "text/html", "UTF-8");
			actionbar.setProgressBarVisibility(View.GONE);
		}
	}

	private void foo() {
		try {
			final URL url = new URL("http://snyamathi.dyndns.org/lithub/?model=LoginPage&view=LoginPageXML&action=login&username=lunin&password=password");
			final SAXParserFactory spf = SAXParserFactory.newInstance();
			final SAXParser sp = spf.newSAXParser();	
			final XMLReader xr = sp.getXMLReader();	
			final PredictionHandler handler = new PredictionHandler();
			xr.setContentHandler(handler);
			Log.d(TAG, "Running parser from foo()");
			xr.parse(new InputSource(url.openStream()));
		} catch (Exception e) {
			Log.e("TAG", e.getMessage());
		}
	}

	private void bar() {
		try {
			final URL url = new URL("http://snyamathi.dyndns.org/lithub/?model=UserPage&view=UserPageXML&action=LendItem&bookTitle=It&usernameOfReceiver=zelus&mobile=WHAHAHA&username=lunin&password=password");
			final SAXParserFactory spf = SAXParserFactory.newInstance();
			final SAXParser sp = spf.newSAXParser();	
			final XMLReader xr = sp.getXMLReader();	
			final PredictionHandler handler = new PredictionHandler();
			xr.setContentHandler(handler);
			Log.d(TAG, "Hitting: " + url.toString());
			xr.parse(new InputSource(url.openStream()));
		} catch (Exception e) {
			Log.e("TAG", e.getMessage());
		}
	}

	private class PredictionHandler extends DefaultHandler {

		public PredictionHandler() {

		}

		@Override
		public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException {
			Log.d(TAG, "Opening tag: " + qName);
		}

		@Override 
		public void characters(char ch[], int start, int length) { 
			String chars = new String(ch, start, length);
			Log.d(TAG, "Text: " + chars);
		}
	}
}