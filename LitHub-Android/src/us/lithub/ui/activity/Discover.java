package us.lithub.ui.activity;

import us.lithub.R;
import us.lithub.ui.TabRecentlyAdded;
import us.lithub.ui.TabRecentlyLent;
import us.lithub.ui.TabRecentlyStarred;
import android.app.TabActivity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.widget.TabHost;

public class Discover extends TabActivity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_discover);
		
		Resources res = getResources(); // Resource object to get Drawables
	    TabHost tabHost = getTabHost();  // The activity TabHost
	    TabHost.TabSpec spec;  // Resusable TabSpec for each tab
	    Intent intent;  // Reusable Intent for each tab    

	    // Initialize a TabSpec for each tab and add it to the TabHost
	    intent = new Intent().setClass(this, TabRecentlyAdded.class);
	    spec = tabHost.newTabSpec("added").setIndicator("Recently Added", res.getDrawable(R.drawable.ic_tab_recently_added)).setContent(intent);
	    tabHost.addTab(spec);

	    intent = new Intent().setClass(this, TabRecentlyStarred.class);
	    spec = tabHost.newTabSpec("albums").setIndicator("Recently Starred", res.getDrawable(R.drawable.ic_tab_recently_starred)).setContent(intent);
	    tabHost.addTab(spec);

	    intent = new Intent().setClass(this, TabRecentlyLent.class);
	    spec = tabHost.newTabSpec("songs").setIndicator("Recently Lent", res.getDrawable(R.drawable.ic_tab_recently_lent)).setContent(intent);
	    tabHost.addTab(spec);

	    tabHost.setCurrentTab(1);
	}
}
