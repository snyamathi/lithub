package us.lithub.ui.activity;

import us.lithub.R;
import us.lithub.Util;
import us.lithub.data.UserDb;
import us.lithub.ui.RowFriend;
import us.lithub.ui.dialog.AddFriend;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class Friends extends Activity {

	private static final int REQUEST_ADD_FRIEND = 1;

	private UserDb mDatabase;
	private ListView mListView;
	private Cursor mCursor;

	// TODO use different modes to do different things on click if necessary
	public static final int PICK_FRIEND_ON_CLICK = 1;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_friends);
		
		if (!Util.isRegistered(this)) {
			Toast.makeText(this, "You must register first (see Settings)", Toast.LENGTH_SHORT).show();
			finish();
		}

		Intent intent = getIntent();
		final int mode = intent.getIntExtra(Util.MODE, 0);
		setResult(RESULT_CANCELED);

		mDatabase = new UserDb(this);
		mDatabase.open();
		mCursor = mDatabase.getAllFriends();
		
		if (mCursor.getCount() == 0) {
			AlertDialog.Builder alertbox = new AlertDialog.Builder(this);
			alertbox.setMessage("You don't have any friends, would you like to add one now?");
			alertbox.setPositiveButton("OK", new DialogInterface.OnClickListener() {  		 

				@Override
				public void onClick(DialogInterface arg0, int arg1) {
					addFriend();
					// TODO reload the cursor instead of just quitting
					finish();
				}
			});
			alertbox.setNegativeButton("No", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					finish();	
				}
			});
			alertbox.show();
		} else {
			FriendAdapter adapter = new FriendAdapter(this, mCursor);

			mListView = (ListView) findViewById(R.id.lvListView);
			mListView.setAdapter(adapter);
			mListView.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
					if (mode == PICK_FRIEND_ON_CLICK) {
						Intent returnIntent = new Intent();

						String friendEmail = mCursor.getString(mCursor.getColumnIndex(UserDb.COLUMN_FRIEND_EMAIL));
						String friendAlias = mCursor.getString(mCursor.getColumnIndex(UserDb.COLUMN_FRIEND_ALIAS));
						String friendDisplayName = mCursor.getString(mCursor.getColumnIndex(UserDb.COLUMN_FRIEND_DISPLAY_NAME));

						returnIntent.putExtra(UserDb.COLUMN_FRIEND_EMAIL, friendEmail);
						returnIntent.putExtra(UserDb.COLUMN_FRIEND_ALIAS, friendAlias);
						returnIntent.putExtra(UserDb.COLUMN_FRIEND_DISPLAY_NAME, friendDisplayName);

						setResult(Activity.RESULT_OK, returnIntent);
						finish();
					} else {
						Toast.makeText(Friends.this, "Click", Toast.LENGTH_SHORT).show();
					}
				}
			});
		}
	}

	private void addFriend() {
		Intent intent = new Intent(this, AddFriend.class);
		startActivityForResult(intent, REQUEST_ADD_FRIEND);
	}

	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		if (requestCode == REQUEST_ADD_FRIEND) {
			if (resultCode == RESULT_OK) {

			} else if (resultCode == RESULT_CANCELED) {

			}
		}
	}

	private class FriendAdapter extends CursorAdapter {

		public FriendAdapter(Context context, Cursor cursor) {
			super(context, cursor);
		}

		@Override
		public void bindView(View view, Context context, Cursor cursor) {
			String name = cursor.getString(cursor.getColumnIndex(UserDb.COLUMN_FRIEND_ALIAS));
			((RowFriend) view).setName(name);

			String emailAddress = cursor.getString(cursor.getColumnIndex(UserDb.COLUMN_FRIEND_EMAIL));
			((RowFriend) view).setEmailAddress(emailAddress);

			// TODO get the url for the image of the friend and pass it in
			// String isbn = cursor.getString(cursor.getColumnIndex(GlobalDb.COLUMN_ISBN));
			((RowFriend) view).setImage();

			/*
			Integer isFavorite = cursor.getInt(cursor.getColumnIndex(UserDb.COLUMN_IS_FAVORITE));
			if (isFavorite == null || isFavorite == 0) {
				((RowBook) view).setIsFavorite(false);
			} else {
				((RowBook) view).setIsFavorite(true);
			}
			 */
		}

		@Override
		public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
			LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View view = layoutInflater.inflate(R.layout.row_friend, null);
			bindView(view, context, cursor);
			return view;
		}
	}
}
