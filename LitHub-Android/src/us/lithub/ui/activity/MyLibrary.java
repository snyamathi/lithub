package us.lithub.ui.activity;

import us.lithub.R;
import us.lithub.Util;
import us.lithub.data.GlobalDb;
import us.lithub.data.UserDb;
import us.lithub.ui.RowBook;
import us.lithub.ui.dialog.PickFriend;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class MyLibrary extends Activity {

	private static final String TAG = MyLibrary.class.getSimpleName();
	
	private UserDb mDatabase;
	private ListView mListView;
	private Cursor mCursor;

	private static final int REQUEST_PICK_FRIEND = 1;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.d(TAG, "starting MyLibrary activity");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_my_library);
		mDatabase = new UserDb(this);
		mDatabase.open();
		mCursor = mDatabase.getAllBooks();	

		// Open up the database and query for all books
		mDatabase = new UserDb(this);
		mDatabase.open();
		mCursor = mDatabase.getAllBooks();	
		
		// Create the adapter and bind data to the list view
		BookAdapter adapter = new BookAdapter(this, mCursor);
		mListView = (ListView) findViewById(R.id.lvListView);
		mListView.setAdapter(adapter);
		mListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				Intent intent = new Intent(MyLibrary.this, PickFriend.class);
				intent.putExtra("ISBN", mCursor.getString(mCursor.getColumnIndex(UserDb.COLUMN_ISBN)));
				startActivityForResult(intent, REQUEST_PICK_FRIEND);				
			}
		});
	}

	private void lendBook(String isbn) {
		if (!currentHaveBook(isbn)) {
			// TODO we don't have the book, show an error
			Toast.makeText(this, "You don't currently have this book in your posession", Toast.LENGTH_SHORT).show();
			return;
		}

		Intent intent = new Intent(this, Friends.class);
		intent.putExtra(Util.MODE, Friends.PICK_FRIEND_ON_CLICK);
		startActivityForResult(intent, REQUEST_PICK_FRIEND);

		// TODO store result in local database
		// TODO display final notification to user
	}

	private class BookAdapter extends CursorAdapter {

		public BookAdapter(Context context, Cursor cursor) {
			super(context, cursor);
		}

		@Override
		public void bindView(View view, Context context, Cursor cursor) {
			String author = cursor.getString(cursor.getColumnIndex(GlobalDb.COLUMN_AUTHOR));
			((RowBook) view).setAuthor(author);

			String title = cursor.getString(cursor.getColumnIndex(GlobalDb.COLUMN_TITLE));
			((RowBook) view).setTitle(title);

			String isbn = cursor.getString(cursor.getColumnIndex(GlobalDb.COLUMN_ISBN));
			((RowBook) view).setIsbn(isbn);
			((RowBook) view).setImage(isbn);

			Integer isFavorite = cursor.getInt(cursor.getColumnIndex(UserDb.COLUMN_IS_FAVORITE));
			if (isFavorite == null || isFavorite == 0) {
				((RowBook) view).setIsFavorite(false);
			} else {
				((RowBook) view).setIsFavorite(true);
			}
		}

		@Override
		public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
			LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View view = layoutInflater.inflate(R.layout.row_book, null);
			bindView(view, context, cursor);
			return view;
		}
	}

	private void confirmLend(final String friendEmailAddress, String friendAlias) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("Are you sure you want to lend this book to " + friendAlias + "?")
		.setCancelable(false)
		.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				new Thread() {
					@Override
					public void run() {

					}
				}.start();
				dialog.cancel();
			}
		})
		.setNegativeButton("No", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	}

	private boolean currentHaveBook(String isbn) {
		UserDb db = new UserDb(this);
		db.open();
		boolean isHeld = db.isBookCurrentlyHeld(isbn);
		db.close();
		return isHeld;
	}

	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		if (requestCode == REQUEST_PICK_FRIEND) {
			if (resultCode != Activity.RESULT_OK) {
				Toast.makeText(this, "Book not lent", Toast.LENGTH_SHORT).show();
				return;
			}

			String friendEmailAddress = intent.getStringExtra(UserDb.COLUMN_FRIEND_EMAIL);
			String friendAlias = intent.getStringExtra(UserDb.COLUMN_FRIEND_ALIAS);
			confirmLend(friendEmailAddress, friendAlias);
		}
	}
}
