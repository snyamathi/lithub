package us.lithub.ui.activity;

import us.lithub.R;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class Register extends Activity {
	
	private static final String TAG = Register.class.getSimpleName();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.d(TAG, "Sarting registration activity");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);
	}
	
	public void okay(View view) {
		EditText edtEmail = (EditText) findViewById(R.id.edtEmail);
		EditText edtUsername = (EditText) findViewById(R.id.edtUsername);
		EditText edtPassword = (EditText) findViewById(R.id.edtPassword);
		EditText edtConfirmPassword = (EditText) findViewById(R.id.edtConfirmPassword);
		
		String email = edtEmail.getText().toString();
		String username = edtUsername.getText().toString();
		String password = edtPassword.getText().toString();
		String confirmPassword = edtConfirmPassword.getText().toString();
		
		if (email.trim().length() == 0) {
			Toast.makeText(this, R.string.toast_email_empty, Toast.LENGTH_SHORT).show();
		} else if (username.trim().length() == 0) {
			Toast.makeText(this, R.string.toast_username_empty, Toast.LENGTH_SHORT).show();
		} else if (password.length() == 0) {
			Toast.makeText(this, R.string.toast_password_empty, Toast.LENGTH_SHORT).show();
		} else if (!password.equals(confirmPassword)) {
			Toast.makeText(this, R.string.toast_passwords_dont_match, Toast.LENGTH_SHORT).show();
		}
		
		register(email, username, password);
	}
	
	public void cancel(View view) {
		finish();
	}
	
	private void register(String email, String username, String password) {
		// TODO register
	}
}