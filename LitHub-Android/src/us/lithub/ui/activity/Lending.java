package us.lithub.ui.activity;

import us.lithub.R;
import us.lithub.data.GlobalDb;
import us.lithub.data.UserDb;
import us.lithub.ui.RowTransaction;
import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class Lending extends Activity {

	private UserDb mDatabase;
	private ListView mListView;
	private Cursor mCursor;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_lending);
		
		mDatabase = new UserDb(this);
		mDatabase.open();
		mCursor = mDatabase.getAllTransactions();
		
		TransactionAdapter adapter = new TransactionAdapter(this, mCursor);
		
		mListView = (ListView) findViewById(R.id.lvListView);
		mListView.setAdapter(adapter);
		mListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				Toast.makeText(Lending.this, "onItemClick()", Toast.LENGTH_SHORT).show();
			}
		});
	}
	
	private class TransactionAdapter extends CursorAdapter {

		public TransactionAdapter(Context context, Cursor cursor) {
			super(context, cursor);
		}

		@Override
		public void bindView(View view, Context context, Cursor cursor) {
			((RowTransaction) view).setFriendsName("Friends Name");
			((RowTransaction) view).setTitle("Book title");

			String isbn = cursor.getString(cursor.getColumnIndex(GlobalDb.COLUMN_ISBN));
			((RowTransaction) view).setIsbn(isbn);
			((RowTransaction) view).setImage(isbn);
		}

		@Override
		public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
			LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View view = layoutInflater.inflate(R.layout.row_transaction, null);
			bindView(view, context, cursor);
			return view;
		}
	}
}
