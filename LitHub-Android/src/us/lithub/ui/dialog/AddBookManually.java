package us.lithub.ui.dialog;

import us.lithub.R;
import android.app.Activity;
import android.os.Bundle;

public class AddBookManually extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dialog_add_book_manually);
		setResult(RESULT_CANCELED);
	}
}
