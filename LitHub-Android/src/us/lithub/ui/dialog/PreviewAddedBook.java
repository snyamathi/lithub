package us.lithub.ui.dialog;

import us.lithub.R;
import us.lithub.Util;
import us.lithub.data.Book;
import us.lithub.data.ItemLookup;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class PreviewAddedBook extends Activity {

	private Intent data;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dialog_preview_added_book);

		// Get the book information via the upc passed in from the intent
		data = getIntent();
		String upc = data.getStringExtra(Book.UPC);
		Book book = ItemLookup.findProduct(upc);
		data.putExtra(Book.ISBN, book.getIsbn());
		
		// Set the text of the various view components
		((TextView) findViewById(R.id.Title)).setText(book.getTitle());
		((TextView) findViewById(R.id.Author)).setText(book.getAuthor());
		((TextView) findViewById(R.id.Publisher)).setText(book.getPublisher());
		((TextView) findViewById(R.id.Summary)).setText(book.getSummary());
		
		// Lazy load the cover image
		ImageView image = (ImageView)findViewById(R.id.imview);
		Util.getImageLoader(this).DisplayImage("http://covers.openlibrary.org/b/isbn/"+upc+"-M.jpg", image);
	}

	public void addBook(View v) {
		setResult(RESULT_OK, data);
		finish();
	}

	public void cancel(View v) {
		finish();
	}
}
