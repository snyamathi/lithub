package us.lithub.ui.dialog;

import us.lithub.R;
import us.lithub.Util;
import us.lithub.client.MyRequestFactory;
import us.lithub.data.UserDb;
import us.lithub.shared.FriendProxy;
import us.lithub.shared.LitHubRequest;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.web.bindery.requestfactory.shared.Receiver;
import com.google.web.bindery.requestfactory.shared.ServerFailure;

public class PickFriend extends Activity {
	
	private static final String TAG = PickFriend.class.getSimpleName();
	
	private int state = INITIAL;
	private Button btnOkay;
	private Button btnCancel;
	private EditText edtEmailAddress;
	private TextView lblStatus;
	
	private static final int INITIAL = 0;
	private static final int SENDING = 1;
	private static final int SUCCESS = 2;
	private static final int NO_USER = 3;
	private static final int FAILURE = 4;
	
	private FriendProxy friendProxy;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dialog_add_friend);
		setResult(Activity.RESULT_CANCELED);
		
		btnOkay = (Button) findViewById(R.id.btnOkay);
		btnCancel = (Button) findViewById(R.id.btnCancel);
		edtEmailAddress = (EditText) findViewById(R.id.edtEmailAddress);
		lblStatus = (TextView) findViewById(R.id.lblStatus);
	}
	
	private void addFriend() {
		new Thread() {
			@Override
			public void run() {
				Message message = new Message();
				message.arg1 = SENDING;
				updateHandler.sendMessage(message);
				String emailAddress = edtEmailAddress.getText().toString();	
				MyRequestFactory requestFactory = Util.getRequestFactory(PickFriend.this, MyRequestFactory.class);
				LitHubRequest request = requestFactory.litHubRequest();
		        FriendProxy proxy = request.create(FriendProxy.class);
		        proxy.setFriendsEmailAddress(emailAddress);    
		        request.updateFriend(proxy).fire(addFriendReceiver);
			}
		}.start();
	}
	
	public void okay(View view) {
		// TODO check that the email is valid before going further
		// TODO check to see if we're already friends with that person
		
		switch (state) {
		case INITIAL:
			addFriend();
			return;
		case SUCCESS:
			setResult(Activity.RESULT_OK);
		case FAILURE:
		case NO_USER:
		default:
			finish();
		}

	}
	
	public void cancel(View view) {
		finish();
	}
	
	private Handler updateHandler = new Handler() {
		@Override
		public void handleMessage (Message msg) {
			state = msg.arg1;
			switch (msg.arg1) {
			case SENDING:
				btnOkay.setEnabled(false);
				btnCancel.setEnabled(false);
				edtEmailAddress.setEnabled(false);
				lblStatus.setText("Checking for user...");
				return;
			case SUCCESS:
				UserDb db = new UserDb(PickFriend.this);
				db.open();
				db.addFriend(friendProxy.getEmailAddress(), friendProxy.getAlias());
				db.close();
				
				btnOkay.setEnabled(true);
				lblStatus.setText("Friend added successfully!");
				return;
			case NO_USER:
				btnOkay.setEnabled(true);
				btnCancel.setEnabled(true);
				edtEmailAddress.setEnabled(true);
				lblStatus.setText("Error: No such user is registered");
				return;
			case FAILURE:
				btnOkay.setEnabled(false);
				btnCancel.setEnabled(false);
				edtEmailAddress.setEnabled(false);
				lblStatus.setText("Error: Unable to communicate with server");
				return;
			}
		}
	};
	
	private Receiver<FriendProxy> addFriendReceiver = new Receiver<FriendProxy>() {
    	
		@Override
		public void onSuccess(FriendProxy response) {
			Log.d(TAG, "Friend added to AppEngine successfully");
			if (response != null) {
				Log.d(TAG, "Friend successfully added to Datastore");
				friendProxy = response;
				Message message = new Message();
				message.arg1 = SUCCESS;
				updateHandler.sendMessage(message);
			} else {
				Log.d(TAG, "No such user exists in Datastore");
				Message message = new Message();
				message.arg1 = NO_USER;
				updateHandler.sendMessage(message);
			}
		}
		
        @Override
        public void onFailure(ServerFailure error) {
        	Log.e(TAG, "Failure to communicate with datastore");
			Message message = new Message();
			message.arg1 = FAILURE;
			updateHandler.sendMessage(message);
        }
	};
}