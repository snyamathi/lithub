package us.lithub.ui;

import us.lithub.R;
import us.lithub.Util;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class RowTransaction extends RelativeLayout {
	
	private Context mContext;
	private String mIsbn;
	
	public RowTransaction(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.mContext = context;
	}
	
	public void setFriendsName(String author) {
		TextView tv = (TextView) findViewById(R.id.lblAuthor);
		tv.setText(author);
	}
	
	public void setTitle(String title) {
		TextView tv = (TextView) findViewById(R.id.lblTitle);
		tv.setText(title);
	}
	
	public void setStatus(int status) {
		ImageView iv = (ImageView) findViewById(R.id.imgFavorite);
		int visibility = status == 100 ? VISIBLE : INVISIBLE;
		iv.setVisibility(visibility);
	}
	
	public void setImage(String isbn) {
		ImageView iv = (ImageView) findViewById(R.id.imgIcon);
		Util.getImageLoader(mContext).DisplayImage("http://covers.openlibrary.org/b/isbn/" + isbn + "-M.jpg", iv);
		if( Util.getImageLoader(mContext).equals(null) )
			iv.setImageResource(R.drawable.generic_book);
	}
	
	public void setIsbn(String isbn) {
		this.mIsbn = isbn;
	}

	public String getIsbn() {
		return mIsbn;
	}
}