package us.lithub.ui;

import java.util.List;

import us.lithub.R;
import us.lithub.Util;
import us.lithub.client.MyRequestFactory;
import us.lithub.shared.DiscoverRequest;
import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.web.bindery.requestfactory.shared.Receiver;
import com.google.web.bindery.requestfactory.shared.ServerFailure;

public class TabRecentlyAdded extends Activity {

	private static final String TAG = TabRecentlyAdded.class.getSimpleName();

	private final Context mContext = this;
	private ListView mListView;

	private Receiver<List<String>> receiver = new Receiver<List<String>>() {

		@Override
		public void onSuccess(final List<String> response) {
			// You have your list of ISBNs, we can now display them in the ListView
			// BUT that needs to be done on the UI thread
			runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					RecentlyAddedListAdapter adapter = new RecentlyAddedListAdapter(mContext, R.layout.row_recently_added, response);
					mListView.setAdapter(adapter);
					mListView.setOnItemClickListener(new OnItemClickListener() {
						
						@Override
						public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
							Toast.makeText(mContext, response.get(position), Toast.LENGTH_SHORT).show();					
						}
					});
					
				}
			});
		}

		@Override
		public void onFailure(ServerFailure error) {
			// Oh noes! Something went wrong
			Log.e(TAG, "Failure to communicate with datastore");
		}
	};
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tab_discover);

		mListView = (ListView) findViewById(R.id.lvListView);

		new GetRecentlyAddedBooksTask().execute();
	}

	private class GetRecentlyAddedBooksTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute () {
			// Do anything that you want to happen on the UI thread BEFORE starting
			// the long running operation to start running here
		}

		@Override
		protected Void doInBackground(Void... params) {
			// Do your long running operation here

			// Get the request factory
			MyRequestFactory requestFactory = Util.getRequestFactory(TabRecentlyAdded.this, MyRequestFactory.class);

			// Get the discover request
			DiscoverRequest request = requestFactory.discoverRequest();

			// Fire off a request for the newest added books and have the reciever listen for the response
			request.getNewelyAddedBooks().fire(receiver);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// Do anything that you want to happen on the UI thread AFTER starting
			// the long running operation to start running here
		}
	}

	private class RecentlyAddedListAdapter extends ArrayAdapter<String> {

		private List<String> isbns;

		public RecentlyAddedListAdapter(Context context, int textViewResourceId, List<String> isbns) {
			super(context, textViewResourceId, isbns);
			this.isbns = isbns;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View v = convertView;

			if (v == null) {
				LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				v = vi.inflate(R.layout.row_recently_added, null);
			}

			String isbn = isbns.get(position);

			if (isbn != null) {

				TextView txtText = (TextView) v.findViewById(R.id.lblText);
				if (txtText != null) {
					txtText.setText(isbn);                            
				}
			}
			return v;
		}

	}
}
