package us.lithub.ui;

import us.lithub.R;
import us.lithub.Util;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class RowFriend extends RelativeLayout {
	
	private Context mContext;
	private String emailAddress;
	
	public RowFriend(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.mContext = context;
	}

	public void setImage() {
		ImageView iv = (ImageView) findViewById(R.id.imgIcon);
		Util.getImageLoader(mContext).DisplayImage("http://dl.dropbox.com/u/9649335/868/default_user_thumb.png", iv);
	}
	
	public void setName(String name) {
		TextView tv = (TextView) findViewById(R.id.lblName);
		tv.setText(name);
	}
	
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	
	public String getEmailAddress() {
		return emailAddress;
	}
}