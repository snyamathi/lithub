package us.lithub.ui;

import us.lithub.R;
import us.lithub.Util;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class RowBook extends RelativeLayout {
	
	private Context mContext;
	private String mIsbn;
	
	public RowBook(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.mContext = context;
	}
	
	public void setAuthor(String author) {
		TextView tv = (TextView) findViewById(R.id.lblAuthor);
		tv.setText(author);
	}
	
	public void setTitle(String title) {
		TextView tv = (TextView) findViewById(R.id.lblTitle);
		tv.setText(title);
	}
	
	public void setIsFavorite(boolean isFavorite) {
		ImageView iv = (ImageView) findViewById(R.id.imgFavorite);
		int visibility = isFavorite ? VISIBLE : INVISIBLE;
		iv.setVisibility(visibility);
	}
	
	public void setImage(String isbn) {
		ImageView iv = (ImageView) findViewById(R.id.imgIcon);
		Util.getImageLoader(mContext).DisplayImage("http://covers.openlibrary.org/b/isbn/" + isbn + "-M.jpg", iv);
		if( Util.getImageLoader(mContext).equals(null) )
			iv.setImageResource(R.drawable.generic_book);
	}
	
	public void setIsbn(String isbn) {
		this.mIsbn = isbn;
	}

	public String getIsbn() {
		return mIsbn;
	}
}