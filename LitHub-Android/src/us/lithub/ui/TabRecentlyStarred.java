package us.lithub.ui;

import java.util.List;

import us.lithub.R;
import us.lithub.Util;
import us.lithub.client.MyRequestFactory;
import us.lithub.data.Book;
import us.lithub.data.ItemLookup;
import us.lithub.shared.DiscoverRequest;
import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.web.bindery.requestfactory.shared.Receiver;
import com.google.web.bindery.requestfactory.shared.ServerFailure;

public class TabRecentlyStarred extends Activity {

	private static final String TAG = TabRecentlyStarred.class.getSimpleName();

	private final Context mContext = this;
	private ListView mListView;

	private Receiver<List<String>> receiver = new Receiver<List<String>>() {

		@Override
		public void onSuccess(final List<String> response) {
			// You have your list of ISBNs, we can now display them in the ListView
			// BUT that needs to be done on the UI thread
			runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					RecentlyStarredListAdapter adapter = new RecentlyStarredListAdapter(mContext, R.layout.row_recently_starred, response);
					mListView.setAdapter(adapter);
					mListView.setOnItemClickListener(new OnItemClickListener() {
						
						@Override
						public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
							Toast.makeText(mContext, response.get(position), Toast.LENGTH_SHORT).show();					
						}
					});	
				}
			});
		}

		@Override
		public void onFailure(ServerFailure error) {
			// Oh noes! Something went wrong
			Log.e(TAG, "Failure to communicate with datastore");
		}
	};
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tab_discover);

		mListView = (ListView) findViewById(R.id.lvListView);

		new GetRecentlyStarredBooksTask().execute();
	}

	private class GetRecentlyStarredBooksTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute () {
			// Do anything that you want to happen on the UI thread BEFORE starting
			// the long running operation to start running here
		}

		@Override
		protected Void doInBackground(Void... params) {
			// Do your long running operation here

			// Get the request factory
			MyRequestFactory requestFactory = Util.getRequestFactory(TabRecentlyStarred.this, MyRequestFactory.class);

			// Get the discover request
			DiscoverRequest request = requestFactory.discoverRequest();

			// Fire off a request for the newest added books and have the reciever listen for the response
			request.getNewelyStarredBooks().fire(receiver);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// Do anything that you want to happen on the UI thread AFTER starting
			// the long running operation to start running here
		}
	}
	
	private class RowRecentlyStarred extends RelativeLayout{

		private Context mContext;
		
		public RowRecentlyStarred(Context context) {
			super(context);
			this.mContext = context;
			// TODO Auto-generated constructor stub
		}
		
		public void setTitle(String title) {
			TextView tv = (TextView) findViewById(R.id.lblTitle);
			tv.setText(title);
		}
		
		public void setImage(String isbn) {
			ImageView iv = (ImageView) findViewById(R.id.imgIcon);
			Util.getImageLoader(mContext).DisplayImage("http://covers.openlibrary.org/b/isbn/" + isbn + "-M.jpg", iv);
			if( Util.getImageLoader(mContext).equals(null) )
				iv.setImageResource(R.drawable.generic_book);
		}
	}

	private class RecentlyStarredListAdapter extends ArrayAdapter<String> {

		private List<String> isbns;

		public RecentlyStarredListAdapter(Context context, int textViewResourceId, List<String> isbns) {
			super(context, textViewResourceId, isbns);
			this.isbns = isbns;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View v = convertView;

			if (v == null) {
				LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				v = vi.inflate(R.layout.row_book, null);
			}

			String isbn = isbns.get(position);

			if (isbn != null) {
				Book current_book = ItemLookup.findProduct(isbn);
				TextView txtText = (TextView) v.findViewById(R.id.lblText);
				/*if (txtText != null) {
					txtText.setText(current_book.getTitle());                            
				}*/
				//((RowRecentlyStarred) v).setTitle(current_book.getTitle());
				//((RowRecentlyStarred) v).setImage(isbn);
				//ImageView image = (ImageView)findViewById(R.id.imgIcon);
				//Util.getImageLoader(mContext).DisplayImage("http://covers.openlibrary.org/b/isbn/"+isbn+"-M.jpg", image);
				((RowBook) v).setImage(isbn);
				((RowBook) v).setTitle(current_book.getTitle());
				((RowBook) v).setAuthor(current_book.getAuthor());
				((RowBook) v).setIsFavorite(false);
			}
			return v;
		}

	}
}
