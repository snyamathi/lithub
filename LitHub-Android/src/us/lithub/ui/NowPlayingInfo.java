package us.lithub.ui;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import android.util.Log;

public class NowPlayingInfo {

	public static final String TAG = NowPlayingInfo.class.getSimpleName();

	public String getHtml() {
		try {
			JSONArray jsonArray = new JSONArray(readFromGitHubApi());

			//for (int i = 0; i < jsonArray.length(); i++) {
			JSONObject jsonObject = jsonArray.getJSONObject(0);
			JSONObject commit = jsonObject.getJSONObject("commit");
			JSONObject author = commit.getJSONObject("author");

			SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
			Date d = sf.parse(author.getString("date"));
			
			String date = DateFormat.getDateInstance(DateFormat.SHORT, Locale.US).format(d);
			String time = DateFormat.getTimeInstance(DateFormat.SHORT, Locale.US).format(d);
			String message = commit.getString("message");
			String name = author.getString("name");
			//}
			return generateHtml(message, name, date, time);
		} catch (Exception e) {
			return null;
		}
	}

	private String generateHtml(String message, String name, String date, String time) {
		StringBuilder sb = new StringBuilder();
		sb.append("<font color=\"white\"<center><h3>Last commit to GitHub:</h3></center>");
		sb.append(name);
		sb.append(" at ");
		sb.append(date);
		sb.append(' ');
		sb.append(time);
		sb.append("<br>");
		sb.append(message);
		return cleanHtml(sb.toString());
	}
	
	private String cleanHtml(String html) {
		html = html.replaceAll("%", "&#37;");
		return html;
	}

	private String readFromGitHubApi() {
		StringBuilder builder = new StringBuilder();
		HttpClient client = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet("https://api.github.com/repos/snyamathi/LitHub/commits");
		try {
			HttpResponse response = client.execute(httpGet);
			StatusLine statusLine = response.getStatusLine();
			int statusCode = statusLine.getStatusCode();
			if (statusCode == 200) {
				HttpEntity entity = response.getEntity();
				InputStream content = entity.getContent();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(content));
				String line;
				while ((line = reader.readLine()) != null) {
					builder.append(line);
				}
			} else {
				Log.e(TAG, "Failed to download file");
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return builder.toString();
	}
}