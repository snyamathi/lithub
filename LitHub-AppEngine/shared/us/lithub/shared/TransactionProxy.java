package us.lithub.shared;

import java.util.Date;

import com.google.web.bindery.requestfactory.shared.ProxyForName;
import com.google.web.bindery.requestfactory.shared.ValueProxy;

@ProxyForName(value = "us.lithub.server.Transaction", locator = "us.lithub.server.TransactionLocator")
public interface TransactionProxy extends ValueProxy {

	Long getId();

	String getEmailAddress();

	void setEmailAddress(String emailAddress);

	String getFriendsEmailAddress();

	void setFriendsEmailAddress(String friendsEmailAddress);

	String getIsbn();

	void setIsbn(String isbn);

	Integer getStatus();

	void setStatus(Integer status);

	Date getStartDate();

	void setStartDate(Date startDate);

	Date getEndDate();

	void setEndDate(Date endDate);

}
