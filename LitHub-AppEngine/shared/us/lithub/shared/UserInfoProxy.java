package us.lithub.shared;

import com.google.web.bindery.requestfactory.shared.ProxyForName;
import com.google.web.bindery.requestfactory.shared.ValueProxy;

@ProxyForName(value = "us.lithub.server.UserInfo", locator = "us.lithub.server.UserInfoLocator")
public interface UserInfoProxy extends ValueProxy {

	Long getId();

	String getEmailAddress();

	void setEmailAddress(String emailAddress);

	String getDisplayName();

	void setDisplayName(String displayName);

}
