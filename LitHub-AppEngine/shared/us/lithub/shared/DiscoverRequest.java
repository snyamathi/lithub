package us.lithub.shared;

import java.util.List;

import com.google.web.bindery.requestfactory.shared.Request;
import com.google.web.bindery.requestfactory.shared.RequestContext;
import com.google.web.bindery.requestfactory.shared.ServiceName;

@ServiceName(value = "us.lithub.server.DiscoverService", locator = "us.lithub.server.DiscoverServiceLocator")
public interface DiscoverRequest extends RequestContext {

	Request<List<String>> getNewelyAddedBooks();
	
	Request<List<String>> getNewelyStarredBooks();
	
	Request<List<String>> getNewelyLentBooks();

}
