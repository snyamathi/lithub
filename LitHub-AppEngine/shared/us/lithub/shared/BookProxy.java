package us.lithub.shared;

import com.google.web.bindery.requestfactory.shared.ProxyForName;
import com.google.web.bindery.requestfactory.shared.ValueProxy;

@ProxyForName(value = "us.lithub.server.Book", locator = "us.lithub.server.BookLocator")
public interface BookProxy extends ValueProxy {

	Long getId();

	String getIsbn();

	void setIsbn(String isbn);

	String getTitle();

	void setTitle(String title);

	String getAuthor();

	void setAuthor(String author);

}
