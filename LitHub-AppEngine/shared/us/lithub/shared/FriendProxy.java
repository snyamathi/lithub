package us.lithub.shared;

import com.google.web.bindery.requestfactory.shared.ProxyForName;
import com.google.web.bindery.requestfactory.shared.ValueProxy;

@ProxyForName(value = "us.lithub.server.Friend", locator = "us.lithub.server.FriendLocator")
public interface FriendProxy extends ValueProxy {

	Long getId();

	String getEmailAddress();

	void setEmailAddress(String emailAddress);

	String getFriendsEmailAddress();

	void setFriendsEmailAddress(String friendsEmailAddress);

	String getAlias();

	void setAlias(String alias);

}
