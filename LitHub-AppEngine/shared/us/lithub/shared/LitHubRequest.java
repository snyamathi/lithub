package us.lithub.shared;

import java.util.List;

import com.google.web.bindery.requestfactory.shared.Request;
import com.google.web.bindery.requestfactory.shared.RequestContext;
import com.google.web.bindery.requestfactory.shared.ServiceName;

@ServiceName(value = "us.lithub.server.LitHubService", locator = "us.lithub.server.LitHubServiceLocator")
public interface LitHubRequest extends RequestContext {

	Request<UserBookProxy> readUserBook(String isbn);
	
	Request<UserBookProxy> readUserBook(String emailAddress, String isbn);

	Request<Void> updateUserBook(UserBookProxy userbook);

	Request<Void> deleteUserBook(String isbn);

	Request<List<UserBookProxy>> queryUserBooks();
	
	Request<List<UserBookProxy>> queryUserBooks(String emailAddress);

	Request<UserInfoProxy> readUserInfo(String emailAddress);

	Request<Void> updateUserInfo(UserInfoProxy user);

	Request<TransactionProxy> updateTransaction(TransactionProxy transaction);

	Request<FriendProxy> getFriend(String friendsEmailAddress);

	Request<FriendProxy> updateFriend(FriendProxy friend);

	Request<Void> removeFriend(String friendsEmailAddress);

	Request<List<FriendProxy>> getAllFriends();
}
