package us.lithub.shared;

import java.util.Date;

import com.google.web.bindery.requestfactory.shared.ProxyForName;
import com.google.web.bindery.requestfactory.shared.ValueProxy;

@ProxyForName(value = "us.lithub.server.UserBook", locator = "us.lithub.server.UserBookLocator")
public interface UserBookProxy extends ValueProxy {

	Long getId();

	String getEmailAddress();

	void setEmailAddress(String emailAddress);

	String getIsbn();

	void setIsbn(String isbn);

	int getStatus();

	void setStatus(int status);

	public Date getDateAdded();

	void setDateAdded(Date dateAdded);

	Date getDateStarred();

	void setDateStarred(Date dateStarred);

}
