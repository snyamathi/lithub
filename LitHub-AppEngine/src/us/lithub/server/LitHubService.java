package us.lithub.server;

import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import us.lithub.annotation.ServiceMethod;

import com.google.android.c2dm.server.PMF;

public class LitHubService {

	/**
	 * Get the information about the book for the current user by ISBN
	 * @param isbn The ISBN of the book we want the information of
	 * @return A UserBook(Proxy) containing such data, null if no entry with this ISBN
	 */
	@ServiceMethod
	public UserBook readUserBook(String isbn) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Get the information about the book for an arbitrary user by ISBN
	 * @param emailAddress eMail address of the user for which we want the book
	 * @param The ISBN of the book we want the information of
	 * @return A UserBook(Proxy) containing such data, null if no entry with this ISBN
	 */
	@ServiceMethod
	public UserBook readUserBook(String emailAddress, String isbn) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Add or update an entry in the users library
	 * @param userbook The book that we wish to add (or update if the ISBN already exists)
	 */
	@ServiceMethod
	public void updateUserBook(UserBook userbook) {
		// TODO Auto-generated method stub
	}

	/**
	 * Delete the book from the user's library completely
	 * @param isbn the ISBN of the book that we want to get rid of
	 */
	@ServiceMethod
	public void deleteUserBook(String isbn) {
		// TODO Auto-generated method stub

	}
	
	/**
	 * @return The list of books that the current user has in their library
	 */
	@ServiceMethod
	public List<UserBook> queryUserBooks() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Get a list of books for an arbitrary user
	 * @param emailAddress The email of the user whose library we wish to query
	 * @return The list of books that the user has in their library
	 */
	@ServiceMethod
	public List<UserBook> queryUserBooks(String emailAddress) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Get the information about a particular user by their email address
	 * @param emailAddress The email address of the user
	 * @return The information about the user
	 */
	@ServiceMethod
	public UserInfo readUserInfo(String emailAddress) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Add or update a user's information
	 * @param user The user info that we wish to add (or update if the user already exists)
	 */
	@ServiceMethod
	public void updateUserInfo(UserInfo user) {
		// TODO Auto-generated method stub
	}

	/**
	 * Add a transaction or update it if a transaction btween these users for this item already exists
	 * @param transaction The transaction that we wish to add or update
	 */
	@ServiceMethod
	public Transaction updateTransaction(Transaction transaction) {
		transaction.setEmailAddress(Utils.getUserEmail());
		PersistenceManager pm = PMF.get().getPersistenceManager();

		try {
			pm.makePersistent(transaction);
		} finally {
			pm.close();
		}
		
		return transaction;
	}

	/**
	 * Add a friend to your friends list or update the friend if they already exist
	 * @param friend The information about the friend that you wish to add
	 * @return Information about the user including their display name as the alias, null if no such user exits
	 */
	@ServiceMethod
	public Friend updateFriend(Friend friend) {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		Query query = pm.newQuery("select from " + UserInfo.class.getName() + " where emailAddress=='" + friend.getFriendsEmailAddress() +"'");
		
		@SuppressWarnings("unchecked")
		List<UserInfo> list = (List<UserInfo>) query.execute();
		if (list.size() == 0) {
			return null;
		}
		
		friend.setEmailAddress(Utils.getUserId());
		friend.setAlias(list.get(0).getDisplayName());
		
		try {
			pm.makePersistent(friend);
			return friend;
		} finally {
			pm.close();
		}
	}

	/**
	 * Get the information of a friend
	 * @param friendsEmailAddress the email address of the friend
	 * @return The information about the friend
	 */
	@ServiceMethod
	public Friend getFriend(String friendsEmailAddress) {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		try {
			Query query = pm.newQuery("select from " + Friend.class.getName() + " where emailAddress=='" + Utils.getUserEmail() + "' && friendsEmailAddress=='" + friendsEmailAddress +"'");
			@SuppressWarnings("unchecked")
			List<Friend> list = (List<Friend>) query.execute();
			return list.size() == 0 ? null : list.get(0);
		} catch (RuntimeException e) {
			System.out.println(e);
			throw e;
		} finally {
			pm.close();
		}
	}
	
	/**
	 * Remove the current friend from your friends list
	 * @param friendsEmailAddress the email address of the friend that you wish to remove
	 */
	@ServiceMethod
	public void removeFriend(String friendsEmailAddress) {
		// TODO Auto-generated method stub
	}

	/**
	 * @return A list of friends of the current user
	 */
	@ServiceMethod
	public List<Friend> getAllFriends() {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		try {
			Query query = pm.newQuery("select from " + Friend.class.getName() + " where emailAddress=='" + Utils.getUserEmail() + "'");
			@SuppressWarnings("unchecked")
			List<Friend> list = (List<Friend>) query.execute();
			if (list.size() == 0) {
				//Workaround for this issue:
				//http://code.google.com/p/datanucleus-appengine/issues/detail?id=24
				list.size();
			}
			return list;
		} catch (RuntimeException e) {
			System.out.println(e);
			throw e;
		} finally {
			pm.close();
		}
	}
}
