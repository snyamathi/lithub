package us.lithub.server;

import com.google.web.bindery.requestfactory.shared.Locator;


public class UserInfoLocator extends Locator<UserInfo, Void> {

	@Override
	public UserInfo create(Class<? extends UserInfo> clazz) {
		return new UserInfo();
	}

	@Override
	public UserInfo find(Class<? extends UserInfo> clazz, Void id) {
		return create(clazz);
	}

	@Override
	public Class<UserInfo> getDomainType() {
		return UserInfo.class;
	}

	@Override
	public Void getId(UserInfo domainObject) {
		return null;
	}

	@Override
	public Class<Void> getIdType() {
		return Void.class;
	}

	@Override
	public Object getVersion(UserInfo domainObject) {
		return null;
	}

}
