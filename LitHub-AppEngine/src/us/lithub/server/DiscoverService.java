package us.lithub.server;

import java.util.ArrayList;
import java.util.List;

public class DiscoverService {

	private static final List<String> testData;

	static {
		testData = new ArrayList<String>();
		testData.add("9780415914192");
		testData.add("9780415914598");
		testData.add("9780415950503");
		testData.add("9780415952453");
		testData.add("9780415970662");
		testData.add("9780416399707");
		testData.add("9780416408409");
		testData.add("9780416737806");
		testData.add("9780419248903");
		testData.add("9780435037864");
		testData.add("9780439355407");
		testData.add("9780439813785");
		testData.add("9780443068553");
		testData.add("9780444518538");
		testData.add("9780444997128");
		testData.add("9780446576437");
		testData.add("9780465012626");
		testData.add("9780465091263");
		testData.add("9780470392553");
		testData.add("9780470457528");
		testData.add("9780470515976");
		testData.add("9780470548141");
		testData.add("9780470550472");
		testData.add("9780470612620");
	}

	public List<String> getNewelyAddedBooks() {
		return testData;
	}

	public List<String> getNewelyStarredBooks() {
		return testData;
	}

	public List<String> getNewelyLentBooks() {
		return testData;
	}

}
