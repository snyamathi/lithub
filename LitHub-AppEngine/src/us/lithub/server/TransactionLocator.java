package us.lithub.server;

import com.google.web.bindery.requestfactory.shared.Locator;


public class TransactionLocator extends Locator<Transaction, Void> {

	@Override
	public Transaction create(Class<? extends Transaction> clazz) {
		return new Transaction();
	}

	@Override
	public Transaction find(Class<? extends Transaction> clazz, Void id) {
		return create(clazz);
	}

	@Override
	public Class<Transaction> getDomainType() {
		return Transaction.class;
	}

	@Override
	public Void getId(Transaction domainObject) {
		return null;
	}

	@Override
	public Class<Void> getIdType() {
		return Void.class;
	}

	@Override
	public Object getVersion(Transaction domainObject) {
		return null;
	}

}
