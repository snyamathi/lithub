package us.lithub.server;

import com.google.web.bindery.requestfactory.shared.Locator;


public class UserBookLocator extends Locator<UserBook, Void> {

	@Override
	public UserBook create(Class<? extends UserBook> clazz) {
		return new UserBook();
	}

	@Override
	public UserBook find(Class<? extends UserBook> clazz, Void id) {
		return create(clazz);
	}

	@Override
	public Class<UserBook> getDomainType() {
		return UserBook.class;
	}

	@Override
	public Void getId(UserBook domainObject) {
		return null;
	}

	@Override
	public Class<Void> getIdType() {
		return Void.class;
	}

	@Override
	public Object getVersion(UserBook domainObject) {
		return null;
	}

}
