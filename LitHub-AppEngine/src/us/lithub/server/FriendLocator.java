package us.lithub.server;

import com.google.web.bindery.requestfactory.shared.Locator;


public class FriendLocator extends Locator<Friend, Void> {

	@Override
	public Friend create(Class<? extends Friend> clazz) {
		return new Friend();
	}

	@Override
	public Friend find(Class<? extends Friend> clazz, Void id) {
		return create(clazz);
	}

	@Override
	public Class<Friend> getDomainType() {
		return Friend.class;
	}

	@Override
	public Void getId(Friend domainObject) {
		return null;
	}

	@Override
	public Class<Void> getIdType() {
		return Void.class;
	}

	@Override
	public Object getVersion(Friend domainObject) {
		return null;
	}

}
