package us.lithub.server;

import com.google.web.bindery.requestfactory.shared.Locator;


public class BookLocator extends Locator<Book, Void> {

	@Override
	public Book create(Class<? extends Book> clazz) {
		return new Book();
	}

	@Override
	public Book find(Class<? extends Book> clazz, Void id) {
		return create(clazz);
	}

	@Override
	public Class<Book> getDomainType() {
		return Book.class;
	}

	@Override
	public Void getId(Book domainObject) {
		return null;
	}

	@Override
	public Class<Void> getIdType() {
		return Void.class;
	}

	@Override
	public Object getVersion(Book domainObject) {
		return null;
	}

}
